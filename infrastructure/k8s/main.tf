resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "digitalocean_kubernetes_cluster" "k8s_cluster" {
	name    = var.cluster_name
	region  = var.region
	version = var.kubernetes_version
	destroy_all_associated_resources = true

	node_pool {
	name       = var.node_pool_name
	size       = var.node_size
	node_count = var.node_count
	}
}

resource "kubernetes_secret" "docker_registry_secret" {
	metadata {
		name      = "private-registry-secret"
}

data = {
	".dockerconfigjson" = jsonencode({
		auths = {
			"${var.docker_registry_server}" = {
			username = var.docker_username
			password = var.docker_password
			email    = var.docker_email
			auth     = base64encode("${var.docker_username}:${var.docker_password}")
			}
		}
		})
}

type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_config_map" "api_env_config" {
  metadata {
    name = "api-db-config"
  }

  data = {
    DATABASE_CLIENT = "postgres"
    DATABASE_URL = digitalocean_database_cluster.postgres_cluster.private_uri
    STRAPI_ADMIN_CLIENT_URL = "*"
    # It is not recommended to store secrets in a ConfigMap
    JWT_SECRET = var.jwt_secret
	#This is set because load balancer uses https traffic to operate
	NODE_TLS_REJECT_UNAUTHORIZED: "0"
  }
}

output "kubeconfig" {
	value = digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0].raw_config
	sensitive = true
}