resource "gitlab_user_sshkey" "ssh_key" {
  title       = "Terraform SSH Key"
  key         = tls_private_key.ssh_key.public_key_openssh
  depends_on  = [tls_private_key.ssh_key]
}

resource "kubernetes_namespace" "argocd" {
  metadata {
    name = "argocd" 
  }
}

resource "kubernetes_namespace" "datadog" {
  metadata {
    name = "datadog" 
  }
}

resource "helm_release" "argocd" {
  name  = "argocd"

  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  namespace        = kubernetes_namespace.argocd.id
  version          = "6.7.11"
  timeout          = "1500"
  create_namespace = true
}

resource "kubernetes_secret_v1" "ssh_key" {
  metadata {
    name      = "ufirst-repo-ssh-key"
    namespace = kubernetes_namespace.argocd.id
    labels = {
      "argocd.argoproj.io/secret-type" = "repository"
    }
  }

  type = "Opaque"

  data = {
    "sshPrivateKey" = tls_private_key.ssh_key.private_key_pem
    "type"          = "git"
    "url"           = "git@gitlab.com:manueljishi/ufirsrt.git"
    "name"          = "gitlab"
    "project"       = "default"
  }
}

resource "helm_release" "datadog_agent" {
  name       = "datadog-agent"
  chart      = "datadog"
  repository = "https://helm.datadoghq.com"
  version    = "3.10.9"
  namespace = kubernetes_namespace.datadog.id
  set_sensitive {
    name  = "datadog.apiKey"
    value = var.datadog_api_key
  }
  set {
    name  = "datadog.site"
    value = var.datadog_site
  }
  set {
    name  = "datadog.logs.enabled"
    value = true
  }
  set {
    name  = "datadog.logs.containerCollectAll"
    value = true
  }
  set {
    name  = "datadog.leaderElection"
    value = true
  }
  set {
    name  = "datadog.collectEvents"
    value = true
  }
  set {
    name  = "clusterAgent.enabled"
    value = true
  }
  set {
    name  = "clusterAgent.metricsProvider.enabled"
    value = true
  }
  set {
    name  = "networkMonitoring.enabled"
    value = true
  }
  set {
    name  = "systemProbe.enableTCPQueueLength"
    value = true
  }
  set {
    name  = "systemProbe.enableOOMKill"
    value = true
  }
  set {
    name  = "securityAgent.runtime.enabled"
    value = true
  }
  set {
    name  = "datadog.hostVolumeMountPropagation"
    value = "HostToContainer"
  }
}