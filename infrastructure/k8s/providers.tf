terraform {
  backend "http" {
    
  }
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.35"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~> 2.20"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.13.2"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "17.0.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

provider "kubernetes" {
  host                   = digitalocean_kubernetes_cluster.k8s_cluster.endpoint
  client_certificate     = base64decode(digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0].client_certificate)
  client_key             = base64decode(digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0].client_key)
  cluster_ca_certificate = base64decode(digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0].cluster_ca_certificate)
  token                  = digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0].token
}

provider "helm" {
  kubernetes {
  host                   = digitalocean_kubernetes_cluster.k8s_cluster.endpoint
  client_certificate     = base64decode(digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0].client_certificate)
  client_key             = base64decode(digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0].client_key)
  cluster_ca_certificate = base64decode(digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0].cluster_ca_certificate)
  token                  = digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0].token  
  }
}

provider "local" {
}

provider "tls" {
}

provider "gitlab" {
  # GitLab provider allows interactions with the GitLab API
  token = var.gitlab_token
  base_url = var.gitlab_base_url
}