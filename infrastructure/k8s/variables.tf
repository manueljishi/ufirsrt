# CLUSTER PROVISIONING VARIABLES
variable "do_token" {
  description = "DigitalOcean API token"
  type        = string
  sensitive   = true
}

variable "cluster_name" {
  description = "Name of the cluster"
  type        = string
  default     = "ufirst-cluster"
}

variable "region" {
  description = "Region"
  type        = string
  default     = "ams3"
}

variable "kubernetes_version" {
  description = "Version"
  type        = string
  default     = "1.29.5-do.0"
}

variable "node_pool_name" {
  description = "Pool name"
  type        = string
  default     = "ufirst-cluster-pool"
}

variable "node_size" {
  description = "Droplet size for nodes"
  type        = string
  default     = "s-4vcpu-8gb-amd"
}

variable "node_count" {
  description = "Number of nodes"
  type        = number
  default     = 1
}

#POSTGRES DATABASE VARIABLES
variable "engine" {
  description = "Database engine"
  type        = string
  default     = "pg"
}

variable "engine_version" {
  description = "Database engine version"
  type        = string
  default     = "16"
}

variable "size" {
  description = "Size of the database nodes"
  type        = string
  default     = "db-s-1vcpu-1gb"
}

#DROPLET VARIABLES
variable "ssh_key_name" {
  description = "Name of the SSH key"
  type        = string
  default     = "my-ssh-key"
}

variable "ssh_key_path" {
  description = "Path to the SSH public key"
  type        = string
  default     = "~/.ssh/id_rsa.pub"
}

variable "droplet_size" {
  description = "Droplet size"
  type        = string
  default     = "s-1vcpu-2gb"
}

#DOCKER PRIVATE REGISTRY SECRET VARIABLES
variable "docker_registry_server" {
  description = "Private registry where images are stored"
  type        = string
}

variable "docker_username" {
  description = "Docker registry username"
  type        = string
}

variable "docker_password" {
  description = "Docker registry password"
  type        = string
  sensitive   = false
}

variable "docker_email" {
  description = "Docker registry email"
  type        = string
}

#GITLAB TOKEN VARIABLES TO MANAGE SSH KEYS

variable "gitlab_token" {
  description = "GitLab Personal Access Token"
  type        = string
  sensitive   = true
}

variable "gitlab_base_url" {
  description = "GitLab Base URL"
  type        = string
  default     = "https://gitlab.com"
}

#DATADOG VARIABLES
variable "datadog_api_key" {
  description = "Datadog API Key"
  type        = string
  sensitive = true
}

variable "datadog_site" {
  description = "Datadog Site"
  type        = string
  default     = "datadoghq.eu"
}

#JWT SECRET
variable "jwt_secret"{
  description = "JWT secret for the api"
  type        = string
  sensitive   = true
}