resource "digitalocean_database_cluster" "postgres_cluster" {
	name       = var.cluster_name
	engine     = var.engine
	version    = var.engine_version
	size       = var.size
	region     = var.region
	node_count = var.node_count
}