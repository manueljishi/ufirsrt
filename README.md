# Ufirst Devops Assignment

![FoodAdvisor](./foodadvisor.png)

## This project was forked from [Strapi Demo](https://github.com/strapi/foodadvisor)

# How to deploy 

This project creates a kubernetes cluster and a postgres cluster using terraform inside of Gitlab CI/CD

1. Inside the infrastructure/k8s folder create a terraform.tfvars with the following structure:

```
do_token                = "DIGITALOCEAN TOKEN"
docker_registry_server  = "registry.gitlab.com"
docker_username         = "GITLAB USERNAME"
docker_password         = "GITLAB PASS"
docker_email            = "GITLAB EMAIL"
datadog_api_key         = "DATADOG API KEY"
jwt_secret              = "JWT secret used for the api"
```

# Gitlab CI/CD required variables
In order to run this job in a gitlab pipeline we need to add the previous variables 
to the gitlab ci/cd variables, for this we need an administrator to add the variables in gitlab with the name TF_VAR_(VARIABLE_NAME) and the required value

### Extra variables required for Pipeline to work
1. CI_REGISTRY -> registry.gitlab.com
2. CI_REGISTRY_PASSWORD -> The gitlab password for the user or an access token, required for the docker login
3. CI_REGISTRY_USER -> Username of gitlab required for the docker login


# Steps to delete
1. Run terraform destroy from infrastructure/k8s file


# Deployment flow

1. When a push occurs to main, gitlab ci/cd  runst the following steps system runs the testing phase
	1. Runs the testing phase
	2. Uses the terraform gitlab backend in order to read the infrastructure state and performs the required steps to get the resources that we need running
	2. Builds the new docker image only if there are changes to the source code that is used in said image and pushes it to the gitlab docker registry
	4. Argo CD:
		1. When the ArgoCD services are up, use the commands listed on [ArgoCD](#argocd) of the "Initial Applications Run" section to be able to check the ArgoCD UI
		
2. ArgoCD will detect the changes and perform the required actions in order to keep the application running in a desired state

# Initial Applications Run

Create a push to master after configuring the required variables and dependencies in order to run a pipeline that will setup the initial k8s cluster, the database and the first applications that will be running on our project 

## Use remote TF state (in this project the variable TF_STATE_NAME is ufirst)
1. You must be an administrator in the project
2. Go to the gitlab repository side project -> Operate -> Terraform States and select the ufirst state and 
click copy init command in order to initialize a terraform repository in your local machine

## <a name="argocd"></a>After deploying, connect to ArgoCD
Now we need to configure ArgoCD to be able to perform GitOps on our project
We will run the following commands:
```
(1) terraform output -raw kubeconfig > ~/.kube/config (in order to get the cluster config to use kubectl)
(2) kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath={.data.password} | base64 -d > argocd-login.txt (in order to get the default ArgoCD login password, admin user)
(3) kubectl port-forward service/argocd-server 8443:443 -n argocd (so kubectl maps local port 8443 to the service deployed by argocd)
```
Now argocd ui should be running on port 8443 of our local machine, use username admin and the password command (2) gives you to log in

## Run the applications
		
		1. kubectl apply -f intrastructure/applications/longhorn/longhorn.yaml (to create resources for longhorn to provision NFS in the cluster)
		2. Start the longhorn application by manually clicking on the sync button
		3. Now create the strapi ArgoCD application by running 
		4. kubectl apply -f intrastructure/applications/api/strapi.yaml (to create resources for argocd to manage the application)
		

# Access the application

If all the steps have worked correctly, you sould be able to get the LoadBalancer's ip, and access the strapi admin panel by following the url LOADBALANCER:1337/admin
To get the ip run ```kubectl get services``` and copy the load balancer's external ip

# Observability and monitoring
The terraform pipeline automatically deploys a datadog agent in the cluster in order to collect metrics and logs from the server

# High availabiliy features

1. DigitalOcean's managed load balancer ensures that the traffic is balanced between all the pods created by the strapi deployment
2. The strapi deployment makes sure that when a pod dies another one is recreated so it always keeps the required number of replicas in place
3. Having an external database cluster ensures that even if the entire cluster fails, the database stays working and the state is not deleted
4. The longhorn application allows us to have a ReadWriteMany persistent volume which multiple pods can use at the same time without having a single point of failure since this storage is distributed and is not only allocated in one server as NFS would have been